// Copyright 2016 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef COVER_TREE_HPP
#define COVER_TREE_HPP

#include <list>
#include <boost/ptr_container/ptr_vector.hpp>
#include <utility>

#include <memory>

#include <iterator>

#include <cstdint>
#include <cstddef>

#include <algorithm>
#include <functional>

#include <cmath>
#include <limits>

#include <cassert>


namespace cover_tree {
	typedef std::int64_t Level;

	const Level MAX_LEVEL = std::numeric_limits<Level>::max();
	const Level MIN_LEVEL = std::numeric_limits<Level>::min();


	namespace impl {
		/**
		 * This function returns the lowest level l such that the distance
		 * d<=2**l, that is, the function returns the lowest level in a
		 * covering tree such that a node p can cover all nodes q whose
		 * distance d(p,q) is less than d.
		 */
		template<typename Distance>
		Level compute_minimum_cover_level(Distance d);


		/**
		 * This function calculates the cover radius of a node at the given
		 * level.
		 */
		template<typename Distance>
		Distance compute_cover_radius_from_level(Level level);


		template<typename Distance>
		bool is_covered(Level level, Distance d)
		{
			return compute_cover_radius_from_level<Distance>(level) >= d;
		}



		// This class uses the non-virtual interface idiom
		template<class T, class Metric>
		class Node
		{
				typedef Node<T, Metric> ThisType;
			public:
				typedef typename Metric::result_type Distance;

				Node() {}
				Node(const ThisType&) {}
				virtual ~Node() {}

				void operator= (const ThisType&) = delete;

				const T& point() const { return point_impl(); }
				Level level() const { return level_impl(); }

				std::pair<Distance, const T&> find_nearest_neighbor(
					const T& p, Metric d) const;
				std::pair<Distance, const T*> find_nearest_neighbor(
					const T& p, Metric d,
					std::pair<Distance, const T*> nearest_neighbor) const;

				std::size_t num_nodes() const { return num_nodes_impl(); }
				std::size_t height() const { return height_impl(); }


			private:
				virtual const T& point_impl() const = 0;
				virtual Level level_impl() const = 0;

				virtual std::pair<Distance, const T*>
				find_nearest_neighbor_impl(
					const T& p, Metric d,
					std::pair<Distance, const T*> nearest_neighbor) const = 0;

				virtual std::size_t num_nodes_impl() const = 0;
				virtual std::size_t height_impl() const = 0;
		};


		template<class T, class Metric>
		struct InnerNode : public Node<T, Metric>
		{
			typedef typename Metric::result_type Distance;
			typedef std::unique_ptr< Node<T,Metric> > NodePtr;
			typedef boost::ptr_vector< Node<T,Metric> > NodeList;

			explicit InnerNode(const T& point, Level level) :
				point_(point),
				level_(level)
			{}

			virtual ~InnerNode() {}


			void add_child(NodePtr p_child)
			{
				children_.push_back( p_child.release() );
			}


			virtual const T& point_impl() const { return point_; }
			virtual Level level_impl() const { return level_; }

			virtual std::pair<Distance, const T*>
			find_nearest_neighbor_impl(
				const T& p, Metric d,
				std::pair<Distance, const T*> nearest_neighbor) const;

			virtual std::size_t num_nodes_impl() const;
			virtual std::size_t height_impl() const;

			const T point_;
			const Level level_;
			NodeList children_;
		};


		template<class T, class Metric>
		struct LeafNode : public Node<T, Metric>
		{
			typedef typename Metric::result_type Distance;
			typedef std::list<T> PointList;


			explicit LeafNode(const T& point, Level level) :
				point_(point),
				level_(level)
			{}

			virtual ~LeafNode() {}


			void add_point(const T& p);

			template<class Iterator>
			void add_points(Iterator first, Iterator last);


			virtual const T& point_impl() const { return point_; }
			virtual Level level_impl() const { return level_; }

			virtual std::pair<Distance, const T*>
			find_nearest_neighbor_impl(
				const T& p, Metric d,
				std::pair<Distance, const T*> nearest_neighbor) const;

			virtual std::size_t num_nodes_impl() const;
			virtual std::size_t height_impl() const;


			const T point_;
			const Level level_;
			PointList points_;
		};



		template<class T, class Metric>
		std::unique_ptr< Node<T,Metric> > make_tree(
			LeafNode<T, Metric>* p_node, Metric d);


		template<class T, class Metric, class Iterator>
		std::pair<typename Metric::result_type, T>
		find_farthest_point(
			const T& p, Metric d, Iterator first, Iterator last);


		template<class T, class Metric, class Iterator>
		std::pair<typename Metric::result_type, const T&>
		find_closest_point(
			const T& p, Metric d, Iterator first, Iterator last,
			const T& q, typename Metric::result_type d_min);

		template<class T, class Metric, class Iterator>
		std::pair<typename Metric::result_type, const T&>
		find_closest_point(
			const T& p, Metric d, Iterator first, Iterator last)
		{
			typedef typename Metric::result_type Distance;
			Distance d_min = std::numeric_limits<Distance>::d_min();

			return find_closest_point(p, d, first, last, p, d_min);
		}



		template<class T, class Metric>
		std::list<T> find_roots(
			const T& p, Metric d, Level level, std::list<T>* p_points,
			const T& r);

		template<class T, class Metric>
		std::list<T> find_roots(
			const T& p, Metric d, Level level, std::list<T> points)
		{
			std::list<T> roots = find_roots(p, d, level, &points, p);
			roots.push_front(p);

			return roots;
		}


		/**
		 * @pre There are at least two roots. Otherwise, there is nothing to do.
		 */
		template<class T, class Metric, class C>
		void assign_points_to_roots(Metric d,C* p_roots,std::list<T>* p_source);
	}



	template<class T, class Metric>
	struct CoverTree
	{
		private:
			typedef impl::Node<T, Metric> Node;
			typedef impl::LeafNode<T, Metric> LeafNode;

		public:
			typedef T value_type;
			typedef Metric metric;
			typedef typename metric::result_type Distance;


			template<class Iterator>
			explicit CoverTree(
				Iterator first, Iterator last, Metric d = Metric());

			CoverTree(const CoverTree<T, Metric>&) = delete;
			void operator= (const CoverTree<T, Metric>&) = delete;


			std::pair<Distance, const T&> find_nearest_neighbor(
				const T& p) const;


			bool empty() const { return !p_root_; }

			std::size_t num_nodes() const;
			std::size_t height() const;


		private:
			Metric metric_;
			std::unique_ptr<Node> p_root_;
	};


#include <cover_tree_cpp.hpp>

}

#endif
