// Copyright 2016 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <boost/test/unit_test.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/mpl/list.hpp>

#include <algorithm>

#include <random>
#include <limits>

#include <cover_tree.hpp>
#include <point.hpp>
#include <boost/numeric/ublas/matrix_expression.hpp>


typedef boost::mpl::list<int, float> test_types;



namespace cover_tree {
namespace impl
{


BOOST_AUTO_TEST_CASE( cover_radius_level_test )
{
	const float f = 1.99;
	Level level = compute_minimum_cover_level(f);
	BOOST_CHECK_EQUAL( level, 1 );


	unsigned u = compute_cover_radius_from_level<unsigned>(1);
	BOOST_CHECK_EQUAL( u, 2 );

	double d = compute_cover_radius_from_level<double>(-1);
	BOOST_CHECK_EQUAL( d, 0.5 );
}



BOOST_AUTO_TEST_CASE_TEMPLATE(find_nearest_neighbor_simple_test, T, test_types)
{
	Matrix<T> matrix(1, 2, 0);
	matrix(0, 1) = 1;

	const Point<T> p(matrix, 0);
	const Point<T> q(matrix, 1);

	{
		typedef ChebychevMetric<T> Metric;
		typedef typename Metric::result_type Distance;

		Metric d;
		LeafNode<Point<T>, Metric> node(p, 0);

		std::pair<Distance, const Point<T>&> nn =
			node.find_nearest_neighbor(q, d);
		Distance d_qn = nn.first;
		Point<T> n = nn.second;

		BOOST_CHECK_EQUAL( d_qn, 1 );
		BOOST_CHECK_EQUAL( d(p, n), 0 );
	}

	{
		typedef ManhattanMetric<T> Metric;
		typedef typename Metric::result_type Distance;

		Metric d;
		LeafNode<Point<T>, Metric> node(p, 0);

		std::pair<Distance, const Point<T>&> nn =
			node.find_nearest_neighbor(q, d);
		Distance d_qn = nn.first;
		Point<T> n = nn.second;

		BOOST_CHECK_EQUAL( d_qn, 1 );
		BOOST_CHECK_EQUAL( d(p, n), 0 );
	}

}



BOOST_AUTO_TEST_CASE_TEMPLATE( make_tree_simple_test, T, test_types )
{
	Matrix<T> matrix(2, 3, 0);
	matrix(0,1) = 7;
	matrix(0,2) = 6;
	matrix(1,2) = 2;

	const Point<T> p(matrix, 0);
	const Point<T> p1(matrix, 1);
	const Point<T> p2(matrix, 2);

	{
		typedef ChebychevMetric<T> Metric;
		Metric d;

		Level level = compute_minimum_cover_level( std::max(d(p,p1), d(p,p2)) );
		LeafNode<Point<T>, Metric> root(p, level);

		for(std::size_t i = 0; i < matrix.size2(); ++i)
			root.points_.push_back( Point<T>(matrix, i) );

		std::unique_ptr< Node<Point<T>, Metric> > p_child = make_tree(&root, d);

		BOOST_CHECK( p_child );

		Node<Point<T>, Metric>& node = *p_child;
		BOOST_CHECK_EQUAL( d(node.point(), p), 0 );
		BOOST_CHECK_EQUAL( node.height(), 3 );
	}

	{
		typedef ManhattanMetric<T> Metric;
		Metric d;

		Level level = compute_minimum_cover_level( std::max(d(p,p1), d(p,p2)) );
		LeafNode<Point<T>, Metric> root(p, level);

		for(std::size_t i = 0; i < matrix.size2(); ++i)
			root.points_.push_back( Point<T>(matrix, i) );

		std::unique_ptr< Node<Point<T>, Metric> > p_child = make_tree(&root, d);

		BOOST_CHECK( p_child );

		Node<Point<T>, Metric>& node = *p_child;
		BOOST_CHECK_EQUAL( d(node.point(), p), 0 );
		BOOST_CHECK_EQUAL( node.height(), 3 );
	}
}



BOOST_AUTO_TEST_CASE_TEMPLATE( find_farthest_point_simple_test, T, test_types )
{
	Matrix<T> matrix(3, 3, 0);
	matrix(0,1) = 2;
	matrix(0,2) = 1;
	matrix(1,2) = 1;
	matrix(2,2) = 1;

	const Point<T> p(matrix, 0);
	const Point<T> points[] = { Point<T>(matrix, 1), Point<T>(matrix, 2) };

	{
		typedef ChebychevMetric<T> Metric;
		Metric d;

		const std::pair<T, Point<T> > info =
			find_farthest_point(p, d, points+0, points+2);
		BOOST_CHECK_EQUAL( info.first, 2 );
		BOOST_CHECK_EQUAL( d(info.second, points[0]), 0 );
	}

	{
		typedef ManhattanMetric<T> Metric;
		Metric d;

		const std::pair<T, Point<T> > info =
			find_farthest_point(p, d, points+0, points+2);
		BOOST_CHECK_EQUAL( info.first, 3 );
		BOOST_CHECK_EQUAL( d(info.second, points[1]), 0 );
	}
}



BOOST_AUTO_TEST_CASE_TEMPLATE( find_roots_test, T, test_types )
{
	typedef std::list< Point<T> > PointList;

	Matrix<T> matrix(2, 3, 0);
	matrix(0,1) = 7;
	matrix(0,2) = 6;
	matrix(1,2) = 2;

	const Matrix<T> copy(matrix);

	const Point<T> p(matrix, 0);
	const Point<T> p1(matrix, 1);
	const Point<T> p2(matrix, 2);

	std::list< Point<T> > points;
	for(std::size_t i = 0; i < matrix.size2(); ++i)
		points.push_back( Point<T>(matrix, i) );

	{
		Level level = 2;
		auto d = ChebychevMetric<T>();

		PointList roots = find_roots(p, d, level, points);

		BOOST_CHECK_EQUAL( roots.size(), 2 );
		BOOST_CHECK_EQUAL( d(roots.front(), p), 0 );
		BOOST_CHECK_EQUAL( d(roots.back(), p1), 0 );
	}

	{
		Level level = 2;
		auto d = ManhattanMetric<T>();

		PointList roots = find_roots(p, d, level, points);

		const Point<T> q = roots.back();

		BOOST_CHECK_EQUAL( roots.size(), 2 );
		BOOST_CHECK_EQUAL( d(roots.front(), p), 0 );
		BOOST_CHECK_EQUAL( d(roots.back(), p2), 0 );
	}

	// regression test:
	// avoid silent column permutations in the matrix by std::partition
	Matrix<T> diff( matrix - copy );
	BOOST_CHECK_EQUAL( boost::numeric::ublas::norm_frobenius(diff), 0 );
}



BOOST_AUTO_TEST_CASE_TEMPLATE(
	assign_points_to_roots_simple_test, T, test_types)
{
	typedef std::list< Point<T> > PointList;

	Matrix<T> matrix(2, 4, 0);
	matrix(0,1) = 1;
	matrix(0,2) = 2;
	matrix(1,2) = 1;
	matrix(0,3) = -1;
	matrix(1,3) = -1;

	{
		typedef ChebychevMetric<T> Metric;

		const Level level = 0;

		PointList points;
		for(std::size_t i = 0; i < matrix.size2(); ++i)
			points.emplace_back(matrix, i);

		std::list< LeafNode<Point<T>, Metric> > roots;
		roots.emplace_back( Point<T>(matrix,0) , level );
		roots.emplace_back( Point<T>(matrix,1) , level );

		assign_points_to_roots(Metric(), &roots, &points);

		BOOST_CHECK( points.empty() );
		BOOST_CHECK_EQUAL( roots.front().points_.size(), 2 );
		BOOST_CHECK_EQUAL( roots.back().points_.size(), 2 );
	}

	{
		typedef ManhattanMetric<T> Metric;

		const Level level = 1;

		PointList points;
		for(std::size_t i = 0; i < matrix.size2(); ++i)
			points.emplace_back(matrix, i);

		std::list< LeafNode<Point<T>, Metric> > roots;
		roots.emplace_back( Point<T>(matrix,0) , level );
		roots.emplace_back( Point<T>(matrix,1) , level );

		assign_points_to_roots(Metric(), &roots, &points);

		BOOST_CHECK( points.empty() );
		BOOST_CHECK_EQUAL( roots.front().points_.size(), 2 );
		BOOST_CHECK_EQUAL( roots.back().points_.size(), 2 );
	}
}

}


BOOST_AUTO_TEST_CASE_TEMPLATE( empty_test, T, test_types )
{
	std::list< Point<T> > points;

	CoverTree< Point<T>, ChebychevMetric<T> > c(points.cbegin(), points.cend());
	BOOST_CHECK( c.empty() );
	BOOST_CHECK_EQUAL( c.num_nodes(), 0 );
	BOOST_CHECK_EQUAL( c.height(), 0 );

	CoverTree< Point<T>, ManhattanMetric<T> > m(points.cbegin(), points.cend());
	BOOST_CHECK( m.empty() );
	BOOST_CHECK_EQUAL( m.num_nodes(), 0 );
	BOOST_CHECK_EQUAL( m.height(), 0 );
}


BOOST_AUTO_TEST_CASE_TEMPLATE( simple_test, T, test_types )
{
	Matrix<T> matrix(1, 2, 0);
	matrix(0, 1) = 1;

	Point<T> p(matrix, 0);
	Point<T> q(matrix, 1);

	std::list< Point<T> > points;
	points.push_back(p);

	{
		typedef ChebychevMetric<T> Metric;

		CoverTree<Point<T>, Metric> tree( points.cbegin(), points.cend() );

		BOOST_CHECK( !tree.empty() );
		BOOST_CHECK_EQUAL( tree.height(), 1 );
		BOOST_CHECK_EQUAL( tree.num_nodes(), 1 );

		auto nn = tree.find_nearest_neighbor(q);

		BOOST_CHECK_EQUAL( nn.first, 1 );
		BOOST_CHECK( &(nn.second) );

		Metric d;
		Point<T> n = nn.second;

		BOOST_CHECK_EQUAL( n(0), 0 );
		BOOST_CHECK_EQUAL( d(p, n), 0 );
		BOOST_CHECK_EQUAL( d(q, n), 1 );
	}

	{
		typedef ManhattanMetric<T> Metric;

		CoverTree<Point<T>, Metric> tree( points.cbegin(), points.cend() );

		BOOST_CHECK( !tree.empty() );
		BOOST_CHECK_EQUAL( tree.height(), 1 );
		BOOST_CHECK_EQUAL( tree.num_nodes(), 1 );

		auto nn = tree.find_nearest_neighbor(q);

		BOOST_CHECK_EQUAL( nn.first, 1 );
		BOOST_CHECK( &(nn.second) );

		Metric d;
		Point<T> n = nn.second;

		BOOST_CHECK_EQUAL( n(0), 0 );
		BOOST_CHECK_EQUAL( d(p, n), 0 );
		BOOST_CHECK_EQUAL( d(q, n), 1 );
	}
}



template<typename T, class Metric>
struct RandomTest
{
	static void execute()
	{
		typedef std::list< Point<T> > PointList;
		typedef typename Metric::result_type Distance;

		const std::size_t N_ITERATIONS = 100;
		const std::size_t N_POINTS = 100;
		const std::size_t N_TESTS = 101;
		const std::size_t DIMENSION = 8;

		const int a = -(1<<23);
		const int b = +(1<<23);

		for(std::size_t iteration = 0; iteration < N_ITERATIONS; ++iteration)
		{
			std::minstd_rand rand(iteration + 1);
			std::uniform_int_distribution<> dist(a, b);
			auto f = [&rand, &dist] () { return dist(rand); };

			Matrix<T> matrix(DIMENSION, N_POINTS, 0);
			std::generate( matrix.data().begin(), matrix.data().end(), f );

			PointList points;
			for(std::size_t j = 0; j < matrix.size2(); ++j)
				points.push_back( Point<T>(matrix, j) );

			CoverTree<Point<T>, Metric> tree( points.begin(), points.end() );


			Matrix<T> queries(DIMENSION, N_TESTS, 0);
			std::generate( queries.data().begin(), queries.data().end(), f );


			for(std::size_t j = 0; j < queries.size2(); ++j)
			{
				Point<T> p(queries, j);

				// search manually
				Metric d;
				std::pair<Distance, const Point<T>*> nn =
					std::make_pair( d(p, points.front()), &points.front() );

				for(auto it = points.begin(); it != points.end(); ++it)
				{
					const Point<T>& r = *it;
					Distance d_pr = d(p, r);

					if( d_pr > 0 && d_pr < nn.first )
						nn = std::make_pair(d_pr, &r);
				}


				std::pair<Distance, const Point<T>&> nn_ct =
					tree.find_nearest_neighbor(p);

				BOOST_CHECK_EQUAL( nn.first, nn_ct.first );
				BOOST_CHECK_EQUAL( d(*(nn.second), nn_ct.second), 0 );
			}
		}
	}
};

BOOST_AUTO_TEST_CASE_TEMPLATE( random_test, T, test_types )
{
	RandomTest< T, ChebychevMetric<T> >::execute();
	RandomTest< T, ManhattanMetric<T> >::execute();
}

}
