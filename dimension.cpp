// Copyright 2016 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <random>
#include <cmath>

#include <point.hpp>
#include <cover_tree.hpp>

#include <vector>
#include <utility>

#include <type_traits>

#include <ctime>
#include <cstdio>

#include <cassert>



template<typename T> void use(const T&) {}



struct Stats
{
	clock_t cpu_time_setup;
	clock_t cumulative_cpu_time_find;
	std::size_t num_tree_nodes;
	std::size_t tree_height;
};



template<typename T> struct MakeEngine
{
	typedef
		typename std::conditional<
			sizeof(T) <= 4,
			std::mt19937,
			std::mt19937_64>::type
		Type;
};

template<typename T> struct MakeDistribution
{
	typedef
		typename std::conditional<
			std::is_integral<T>::value,
			std::uniform_int_distribution<T>,
			std::uniform_real_distribution<T> >::type
		Type;
};



template<
	typename T, class Metric,
	class Engine = typename MakeEngine<T>::Type,
	class Distribution = typename MakeDistribution<T>::Type>
struct Experiment
{
	typedef typename Metric::result_type Distance;
	typedef cover_tree::CoverTree<Point<T>, Metric> CoverTree;
	typedef std::vector< Point<T> > PointList;


	static Stats execute(
		std::size_t dimension,
		typename Engine::result_type seed, T a, T b,
		std::size_t num_points, std::size_t num_tests)
	{
		assert( dimension > 0 );
		assert( std::isfinite(a) );
		assert( std::isfinite(b) );
		assert( a < b );

		Engine rand(seed);
		Distribution dist(a, b);
		auto f = [&rand, &dist] () { return dist(rand); };


		// make points
		Matrix<T> matrix(dimension, num_points, 0);
		std::generate( matrix.data().begin(), matrix.data().end(), f );

		PointList points;
		points.reserve( matrix.size2() );
		for(std::size_t j = 0; j < matrix.size2(); ++j)
			points.emplace_back(matrix, j);


		// make queries
		Matrix<T> queries(dimension, num_tests, 0);
		std::generate( queries.data().begin(), queries.data().end(), f );


		// setup
		clock_t cpu_time_0 = std::clock();
		CoverTree tree( points.cbegin(), points.cend() );
		clock_t cpu_time_1 = std::clock();


		// solve
		for(std::size_t j = 0; j < queries.size2(); ++j)
		{
			Point<T> p(queries, j);

			std::pair<Distance, const Point<T>&> nn =
				tree.find_nearest_neighbor(p);
			use(nn);
		}
		clock_t cpu_time_2 = std::clock();

		Stats ret = {
			cpu_time_1 - cpu_time_0,
			cpu_time_2 - cpu_time_1,
			tree.num_nodes(),
			tree.height()
		};

		return ret;
	}
};



enum class Metric : char
{
	Manhattan = 'M',
	Euclidean = 'E',
	Chebychev = 'C'
};



template<typename T>
Stats run_experiment(
	std::size_t dimension,
	T seed, T a, T b,
	std::size_t num_points, std::size_t num_queries, Metric metric)
{
	typedef Experiment< T, ManhattanMetric<T> > Experiment_1;
	typedef Experiment< T, EuclideanMetric<T> > Experiment_2;
	typedef Experiment< T, ChebychevMetric<T> > Experiment_inf;

	const std::size_t n = num_points;
	const std::size_t k = num_queries;

	switch(metric)
	{
		case Metric::Manhattan:
			return Experiment_1::execute(dimension, seed, a, b, n, k);
		case Metric::Euclidean:
			return Experiment_2::execute(dimension, seed, a, b, n, k);
		case Metric::Chebychev:
			return Experiment_inf::execute(dimension, seed, a, b, n, k);
	}

	assert(0);
	return Stats();
}



int main()
{
	const std::size_t num_iterations = 25;
	const std::size_t num_queries = 1000;
	const std::size_t dimensions[] = { 10, 12, 14, 16, 18, 20 };
	const std::size_t num_points[] = { 100*1000 };
	const Metric metrics[] =
	{
		Metric::Manhattan, Metric::Euclidean, Metric::Chebychev
	};

	typedef float Number;
	const Number c = 1;
	const Number a = -c;
	const Number b = +c;


	for(Metric metric : metrics)
	{
		for(std::size_t dimension : dimensions)
		{
			for(std::size_t n : num_points)
			{
				for(std::size_t iter = 0; iter < num_iterations; ++iter)
				{
					const Number seed = iter + 1;

					Stats stats = run_experiment(
						dimension, seed, a, b, n, num_queries, metric);

					double cpu_setup
						= 1.0 * stats.cpu_time_setup / CLOCKS_PER_SEC;
					double cpu_find =
						1.0 * stats.cumulative_cpu_time_find / CLOCKS_PER_SEC;

					std::printf(
						"%2zu %c %4zu %8zu %8zu %2zu %6zu %8.2f %8.2f\n",
						iter,
						int(metric),
						dimension, n, stats.num_tree_nodes, stats.tree_height,
						num_queries,
						cpu_setup, cpu_find);
				}
			}
		}
	}
}
