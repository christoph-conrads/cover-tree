// Copyright 2016 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#ifndef POINT_HPP
#define POINT_HPP

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_expression.hpp>

#include <limits>
#include <type_traits>


template<typename T>
using Matrix =
	boost::numeric::ublas::matrix<T, boost::numeric::ublas::column_major>;


template<typename T>
using Point = boost::numeric::ublas::matrix_column< const Matrix<T> >;



template<typename T>
struct ManhattanMetric
{
	typedef T result_type;

	T operator() (const Point<T>& p, const Point<T>& q) const
	{
		return boost::numeric::ublas::norm_1(p-q);
	}
};


template<typename T>
struct EuclideanMetric
{
	static_assert(
		std::is_floating_point<T>::value, "T must be a float type");

	typedef T result_type;

	T operator() (const Point<T>& p, const Point<T>& q) const
	{
		return boost::numeric::ublas::norm_2(p-q);
	}
};


template<typename T>
struct ChebychevMetric
{
	typedef T result_type;

	T operator() (const Point<T>& p, const Point<T>& q) const
	{
		return boost::numeric::ublas::norm_inf(p-q);
	}
};

#endif
