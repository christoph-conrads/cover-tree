// Copyright 2016 Christoph Conrads
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

namespace impl
{


template<typename Distance>
Level compute_minimum_cover_level(Distance d)
{
	assert( d >= 0 );

	Distance cover_level = std::ceil( std::log2(d) );

	assert( cover_level <= MAX_LEVEL );
	assert( cover_level >= MIN_LEVEL );

	return cover_level;
}



template<typename Distance>
Distance compute_cover_radius_from_level(Level level)
{
	assert( level >= 0 || !std::numeric_limits<Distance>::is_integer );

	if( level == MAX_LEVEL )
		return std::numeric_limits<Distance>::is_integer
			? std::numeric_limits<Distance>::max()
			: std::numeric_limits<Distance>::infinity();

	Distance d = std::pow( 2, Distance(level) );

	assert( d > 0 );
	assert( std::isfinite(d) );

	return d;
}



template<class T, class Metric>
std::pair<typename Metric::result_type, const T&>
Node<T, Metric>::find_nearest_neighbor(const T& p, Metric d) const
{
	Distance d_init =
		std::numeric_limits<Distance>::is_integer
		? std::numeric_limits<Distance>::max()
		: std::numeric_limits<Distance>::infinity();

	std::pair<Distance, const T*> init = std::make_pair( d_init, &point() );
	std::pair<Distance, const T*> nn = find_nearest_neighbor(p, d, init);
	// Do not use std::make_pair here because *(nn.second) will be copied onto
	// the stack and then a reference to the stack object will be returned.
	// Thus, ret.second would be an invalid reference after leaving this
	// function.
	std::pair<Distance, const T&> ret( nn.first, *(nn.second) );

	return ret;
}


template<class T, class Metric>
std::pair<typename Metric::result_type, const T*>
Node<T, Metric>::find_nearest_neighbor(
	const T& p, Metric d,
	std::pair<Distance, const T*> nearest_neighbor) const
{
	assert( nearest_neighbor.first > 0 );
	assert( nearest_neighbor.second );

	const T& r = point();
	const Distance d_pr = d(p, r);
	const Distance d_cover = compute_cover_radius_from_level<Distance>(level());


	// the second comparison is sufficient but with the first comparison we can
	// avoid (integer) overflows
	if( (d_cover < d_pr) && (d_pr - d_cover > nearest_neighbor.first) )
		return nearest_neighbor;


	if( d_pr > 0 && d_pr < nearest_neighbor.first )
		nearest_neighbor = std::make_pair(d_pr, &r);

	nearest_neighbor = find_nearest_neighbor_impl(p, d, nearest_neighbor);

	assert( nearest_neighbor.first > 0 );
	assert( std::isfinite(nearest_neighbor.first) );
	assert( nearest_neighbor.second );

	return nearest_neighbor;
}



template<class T, class Metric>
std::pair<typename Metric::result_type, const T*>
InnerNode<T, Metric>::find_nearest_neighbor_impl(
	const T& p, Metric d,
	std::pair<Distance, const T*> nearest_neighbor) const
{
	for(const Node<T, Metric>& child : children_)
		nearest_neighbor = child.find_nearest_neighbor(p, d, nearest_neighbor);

	return nearest_neighbor;
}



template<class T, class Metric>
std::size_t InnerNode<T, Metric>::num_nodes_impl() const
{
	std::size_t s = 0;

	for(const Node<T, Metric>& node : children_)
		s += node.num_nodes();

	return s + 1;
}


template<class T, class Metric>
std::size_t InnerNode<T, Metric>::height_impl() const
{
	std::size_t h = 0;

	for(const Node<T, Metric>& node : children_)
		h = std::max( h, node.height() );

	return h + 1;
}



template<class T, class Metric>
std::pair<typename Metric::result_type, const T*>
LeafNode<T, Metric>::find_nearest_neighbor_impl(
	const T& p, Metric d,
	std::pair<Distance, const T*> nearest_neighbor) const
{
	for(const T& q : points_)
	{
		Distance d_pq = d(p, q);

		if(d_pq < nearest_neighbor.first && d_pq > 0)
			nearest_neighbor = std::make_pair(d_pq, &q);
	}

	return nearest_neighbor;
}


template<class T, class Metric>
void LeafNode<T, Metric>::add_point(const T& p)
{
	points_.push_back(p);
}


template<class T, class Metric>
template<class Iterator>
void LeafNode<T, Metric>::add_points(Iterator first, Iterator last)
{
	points_.insert( points_.end(), first, last );
}


template<class T, class Metric>
std::size_t LeafNode<T, Metric>::num_nodes_impl() const
{
	return points_.size() + 1;
}


template<class T, class Metric>
std::size_t LeafNode<T, Metric>::height_impl() const
{
	return 1;
}



template<class T, class Metric>
std::unique_ptr< Node<T,Metric> > make_tree(
	LeafNode<T, Metric>* const p_node, Metric d)
{
	assert( p_node );

	typedef typename Metric::result_type Distance;
	typedef typename LeafNode<T, Metric>::PointList PointList;
	typedef std::vector< LeafNode<T, Metric> > LeafNodeList;

	const T& p = p_node->point();
	const Level node_level = p_node->level_;
	PointList& points = p_node->points_;
	assert( !points.empty() );

	if( points.size() == 1 )
	{
		assert( d(p, points.front()) == 0 );

		return std::unique_ptr< Node<T, Metric> >(
				new InnerNode<T, Metric>(p, node_level) );
	}


	typedef typename PointList::iterator Iterator;
	Iterator first = points.begin();
	Iterator last = points.end();

	// determine level of roots
	std::pair<Distance, T> farthest_info =
		find_farthest_point(p, d, first, last);
	Distance d_max = farthest_info.first;
	assert( d_max > 0 );

	Level root_level = compute_minimum_cover_level(d_max) - 1;
	assert( root_level < node_level );

	PointList root_points = find_roots(p, d, root_level, points);
	assert( root_points.size() >= 2 );


	// construct root nodes
	LeafNodeList roots;

	for(const T& r : root_points)
		roots.push_back( LeafNode<T, Metric>(r, root_level) );

	assign_points_to_roots(d, &roots, &points);


	// create children
	std::unique_ptr< InnerNode<T,Metric> > p_retval(
		new InnerNode<T, Metric>(p, node_level) );

	for(LeafNode<T, Metric>& root : roots)
	{
		if( d(p, root.point()) == 0 && root.num_nodes() == 1 )
			continue;

		std::unique_ptr< Node<T, Metric> > p_child = make_tree(&root, d);
		p_retval->add_child( std::move(p_child) );
	}

	assert( points.empty() );

	return std::move(p_retval);
}



template<class T, class Metric, class Iterator>
std::pair<typename Metric::result_type, T>
find_farthest_point(
	const T& p, Metric d, Iterator first, Iterator last)
{
	assert( first != last );

	typedef typename Metric::result_type Distance;

	const T* p_far = &(*first);
	Distance d_pf = d(p, *p_far);

	for(Iterator it = std::next(first); it != last; ++it)
	{
		const T& q = *it;
		Distance d_pq = d(p, q);

		if(d_pq <= d_pf)
			continue;

		p_far = &q;
		d_pf = d_pq;
	}

	assert( d_pf > 0 );
	assert( std::isfinite(d_pf) );

	return std::make_pair(d_pf, *p_far);
}



template<class T, class Metric>
std::list<T> find_roots(
	const T& p, Metric d, Level level, std::list<T>* p_points,
	const T& r)
{
	assert( p_points );
	assert( !p_points->empty() );

	typedef typename Metric::result_type Distance;

	auto cannot_be_root = [d, r, level] (const T& q)
	{
		const Distance d_rq = d(r, q);

		return d_rq <= compute_cover_radius_from_level<Distance>(level);
	};

	p_points->remove_if( cannot_be_root );


	if( p_points->empty() )
		return std::list<T>();


	std::pair<Distance, T> farthest_info =
		find_farthest_point(p, d, p_points->begin(), p_points->end());
	const T& s = farthest_info.second;

	std::list<T> roots = find_roots(p, d, level, p_points, s);
	roots.push_back(s);

	return roots;
}



template<class T, class Metric, class C>
void assign_points_to_roots(Metric d, C* p_roots, std::list<T>* p_source)
{
	assert( p_roots );
	assert( p_roots->size() >= 2 );
	assert( p_source );

	if( p_source->empty() )
		return;

	typedef typename Metric::result_type Distance;
	typedef typename LeafNode<T, Metric>::PointList PointList;
	typedef typename C::iterator Iterator;

	while( !p_source->empty() )
	{
		const T p = p_source->front();
		p_source->pop_front();

		Iterator it = p_roots->begin();
		Iterator last = p_roots->end();

		Distance d_min = d(p, it->point());
		Iterator nearest = it;

		for(++it; it != last; ++it)
		{
			const T& r = it->point();
			Distance d_rp = d(r, p);

			if( d_rp >= d_min )
				continue;

			d_min = d_rp;
			nearest = it;
		}

		assert( is_covered(nearest->level_, d_min) );

		PointList& destination = nearest->points_;
		destination.push_back(p);
	}
}

} // namespace



template<class T, class Metric>
template<class Iterator>
CoverTree<T, Metric>::CoverTree(Iterator first, Iterator last, Metric d) :
	metric_(d),
	p_root_(nullptr)
{
	if( first == last )
		return;

	const T& p = *first;

	LeafNode dummy(p, MAX_LEVEL);
	dummy.add_points(first, last);

	std::unique_ptr<Node> p_root = impl::make_tree(&dummy, d);
	assert( p_root );

	p_root_ = std::move(p_root);
}


template<class T, class Metric>
std::pair<typename Metric::result_type, const T&>
CoverTree<T, Metric>::find_nearest_neighbor(const T& p) const
{
	assert( !empty() );

	return p_root_->find_nearest_neighbor(p, metric_);
}


template<class T, class Metric>
std::size_t CoverTree<T, Metric>::num_nodes() const
{
	return p_root_ ? p_root_->num_nodes() : 0;
}


template<class T, class Metric>
std::size_t CoverTree<T, Metric>::height() const
{
	return p_root_ ? p_root_->height() : 0;
}
