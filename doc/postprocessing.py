#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright 2016 Christoph Conrads
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import csv

import numpy as NP
import scipy.optimize as OPT

import copy
import operator

import sys



class Record:
    def __init__(
        self, i, metric, d, n, num_nodes, tree_height, num_queries,
        cpu_time_setup, cpu_time_queries):
        self.i = i
        self.metric = metric
        self.d = d
        self.n = n
        self.num_nodes = num_nodes
        self.tree_height = tree_height
        self.num_queries = num_queries
        self.cpu_time_setup = cpu_time_setup
        self.cpu_time_queries = cpu_time_queries



def read_input(filename):
    def str2metric(c):
        return c

    def row2record(i, row):
        return Record( \
            int(row[0]), str2metric(row[1]), int(row[2]), int(row[3]),
            int(row[4]), int(row[5]), int(row[6]),
            float(row[7]), float(row[8]) )

    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=' ', skipinitialspace=True)
        return map(lambda x: row2record(x[0], x[1]), enumerate(reader))



def main(argv=[]):
    if len(argv) != 2:
        print 'usage: python {0} <csv file>'.format(argv[0])
        return 1

    filename = argv[1]
    records = read_input(filename)

    unique = lambda xs: sorted(list(set(xs)))
    dimensions = unique( map(lambda r: r.d, records) )
    ns = unique( map(lambda r: r.n, records) )
    metrics = unique( map(lambda r: r.metric, records) )
    queries = unique( map(lambda r: r.num_queries, records) )
    iterations = unique( map(lambda r: r.i, records) )

    if len(ns) != 1:
        raise ValueError('len(ns) must be equal to 1')

    if len(queries) != 1:
        raise ValueError('len(queries) must be equal to 1')

    assert len(metrics) == 3

    num_queries = queries[0]
    num_iterations = len(iterations)
    metrics = ['M', 'E', 'C']


    # extract data
    times = \
        NP.full( [2, len(metrics), len(dimensions), num_iterations], NP.nan )

    assert len(ns) == 1
    for i, m in enumerate(metrics):
        for j, d in enumerate(dimensions):
            rs = filter(lambda r: r.d == d and r.metric == m, records)

            times[0,i,j,:] = NP.array( map(lambda r: r.cpu_time_setup, rs) )
            times[1,i,j,:] = NP.array( map(lambda r: r.cpu_time_queries, rs) )

    ds = NP.array(dimensions)

    means = NP.mean(times, axis=-1)
    stds = NP.std(times, axis=-1)


    # fit curves
    fs = [ \
        # does not fit well for Euclidean, Manhattan metric even when
        # considering the standard deviation
        #lambda x, c1, c0: c0 * NP.power(c1, x),
        lambda x, c1, c0: c0 * NP.power(x, c1),
        lambda x, c1, c0: c0 * NP.power(x, c1)
    ]
    params = NP.full( [2, len(metrics), 2], NP.nan )

    for i in [0,1]:
        for j, m in enumerate(metrics):
            params[i,j,:], _ = OPT.curve_fit( \
                fs[i], ds, means[i,j,:],
                sigma=stds[i,j,:], absolute_sigma=True)

    # d n setup[metric means stds fit] queries[metric means stds fit]
    n = ns[0]

    for k, d in enumerate(dimensions):
        print '{:2d} {:8d} {:4d}'.format(d, n, num_queries),

        for i in [0,1]:
            for j, m in enumerate(metrics):
                fmt = '{:6.2f} {:6.2f} {:6.2f}'
                f = fs[i]
                y = f(d, *params[i,j])

                print fmt.format(means[i,j,k], stds[i,j,k], y),

        print

    return 0



if __name__ == '__main__':
    sys.exit( main(sys.argv) )
