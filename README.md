# A Nearest Ancestor Cover Tree in C++14

This repository contains a [nearest ancestor cover tree][NACT] implementation with
batch construction and nearest-neighbor search implemented in C++14 using Boost Test and CMake.

Plots for the measured construction time and the measured search time can be found in the doc/ subdirectory. The measurements were processed with Python 2 and plotted with LaTeX and pgfplots.

[NACT]: <http://www.jmlr.org/proceedings/papers/v37/izbicki15.pdf>
